var knex = require('knex')({
    client: 'pg',
    connection: {
        host: "ec2-54-235-73-241.compute-1.amazonaws.com",
        user: "symfrfctxtoago",
        password: "e456229c072ca4d9042a8ba92cc471b2802404689e0f3f69e81eca15889b9736",
        database: "d495mkooj1dp2h",
        port: 5432,
        ssl: true
    }
});

exports.findById = function(id, cb) {
    process.nextTick(function() {
        knex('users')
            .where({ user_id: id })
            .then(function(pass_id) {
                if (pass_id[0] == undefined) {
                    cb(new Error('User ' + id + ' does not exist'));
                } else {
                    cb(null, pass_id[0]);
                }
            })
    });
}

exports.findByUsername = function(username, cb) {
    process.nextTick(function() {
        knex('users')
            .where({ email: username })
            .select('password', 'user_id')
            .then(function(pass_id) {
                if (pass_id[0] == undefined) {
                    return cb(null, null);
                } else {
                    cb(null, pass_id[0]);
                }
            })
    });
}

//for parent
exports.findAnnounces = function(std_id, cb) {
    process.nextTick(function() {
        knex('student').where({
                s_id: std_id
            }).select('name', 'class_section')
            .then(function(cla) {
                if (cla[0].class_section) {
                    knex('announcement').where({
                            classname: cla[0].class_section
                        }).select('*')
                        .orderBy('ntf_creationdate', 'desc')
                        .then(function(ann) {
                            cb(null, ann);
                        })
                } else {
                    cb(null, null)
                }
            })
    })

}

exports.findBildirim = function(pid, cb) {
    process.nextTick(function() {
        var bildirimler = [];
        knex('student').where({
                p_id: pid
            }).select('s_id')
            .then(function(student) {
                student.forEach(function(value) {
                    knex('bilgilendirme').where({
                            oid: value.s_id
                        }).select('*')
                        .then(function(ann) {
                            ann.forEach(function(dd) {
                                bildirimler.push(dd);
                            })
                            if (student[student.length - 1].s_id == value.s_id) {
                                cb(null, bildirimler)
                            }
                        })
                })
            })
    })

}

exports.findQuestion = function(user_id, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                writer_id: user_id
            })
            .select('*')
            .orderBy('ntf_creationdate', 'desc')
            .then(function(ann) {
                cb(null, ann);
            })
    })
}

exports.findQuestionT = function(user_id, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                teacher: user_id
            })
            .select('*')
            .orderBy('ntf_creationdate', 'desc')
            .then(function(ann) {
                cb(null, ann);
            })
    })
}

exports.devamsizlik = function(s_id, date, dersSaati, cb) {
    process.nextTick(function() {
        knex('bilgilendirme')
            .insert({
                oid: s_id,
                tarih: date,
                derssaati: dersSaati
            })
            .then(function(ann) {
                return cb(null, ann)
            })
    })
}

exports.devamsizlikList = function(yoklama, date, dersSaati, cb) {
    process.nextTick(function() {
        knex('devamsizliklist')
            .insert({
                tarih: date,
                yoklama: yoklama,
                derssaati: dersSaati
            })
            .then(function(ann) {
                return cb(null, ann)
            })
    })
}

//for teacher
exports.findAnn = function(clas, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                classname: clas[0]
            })
            .select('*')
            .orderBy('ntf_creationdate', 'desc')
            .then(function(ann) {
                cb(null, ann);
            })
    })
}

exports.getAnnByID = function(id, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                ntf_id: id
            })
            .select('*')
            .orderBy('ntf_creationdate', 'desc')
            .then(function(ann) {
                cb(null, ann);
            })
    })
}

exports.getCommentType = function(id, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                ntf_id: id
            })
            .select('ntf_tag')
            .then(function(ann) {
                cb(null, ann[0].ntf_tag);
            })
    })
}

exports.deleteAnnByID = function(id, cb) {
    process.nextTick(function() {
        knex('comment')
            .where({ notf_id: id })
            .del()
            .then(function() {
                knex('announcement')
                    .where({
                        ntf_id: id
                    })
                    .del()
                    .then(function() {
                        cb(null, null);
                    })
            })
    })
}

exports.editAnnByID = function(id, title, text, cb) {
    process.nextTick(function() {
        knex('announcement')
            .update({
                subject: title,
                text_box: text,
                classname: '3-A'
            }).where({ ntf_id: id })
            .then(function(std) {
                return cb(null, null)
            })

    })
}

exports.findPuppies = function(user_id, ch_no, cb) {
    process.nextTick(function() {
        knex('parent').where({
                p_id: user_id
            }).select('std_id')
            .then(function(std) {
                knex('teacher').where({
                        t_id: user_id
                    }).select('courselist')
                    .options({ rowMode: 'array' })
                    .then(function(clas) {
                        if (clas[0] != undefined) {
                            return cb(null, null, clas[0][0]);
                        } else if (std[0].std_id != null && std[0] != null && std[0].std_id[0] != null) {
                            cb(null, std[0], null);
                        } else {
                            cb(null, null, null);
                        }
                    })
            })
    })
}

exports.findNames = function(sutd, cb) {
    process.nextTick(function() {
        knex('student').where({
                s_id: sutd
            }).select('name', 'surname')
            .then(function(std) {
                cb(null, std[0]);
            })
    })
}

exports.postAnnounces = function(user_id, username, title, text, date, type, cb) {
    process.nextTick(function() {
        knex('announcement')
            .insert({
                subject: title,
                text_box: text,
                writer_id: user_id,
                ntf_creationdate: date,
                ntf_tag: type,
                classname: '3-A',
                writer_name: username
            })
            .returning('*')
            .then(function(ann) {
                return cb(null, ann)
            }).catch(function(error) {
                console.log(error)
                cb(error, null);
            })

    })
}

exports.postQuestion = function(user_id, username, t_id, title, text, date, type, cb) {
    process.nextTick(function() {
        knex('announcement')
            .insert({
                subject: title,
                text_box: text,
                writer_id: user_id,
                teacher: t_id,
                ntf_creationdate: date,
                ntf_tag: type,
                writer_name: username
            })
            .returning('*')
            .then(function(ann) {
                return cb(null, ann)
            }).catch(function(error) {
                console.log(error)
                cb(error, null);
            })

    })
}

exports.oyVer = function(evet, hayir, id, ntfid, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                ntf_id: ntfid
            })
            .select('evet')
            .options({ rowMode: 'array' })
            .then(function(ivit) {
                console.log(ivit[0][0])
                if (ivit[0][0] == null) {
                    if (evet == 'evet') {
                        knex('announcement')
                            .where({
                                ntf_id: ntfid
                            })
                            .update({
                                evet: knex.raw('array_append(evet,?)', [id])
                            }).then(function(ann) {
                                cb(null, null);
                            })
                    }
                } else if (ivit[0][0].indexOf(id) == -1) {
                    if (evet == 'evet') {
                        knex('announcement')
                            .where({
                                ntf_id: ntfid
                            })
                            .update({
                                evet: knex.raw('array_append(evet,?)', [id])
                            }).then(function(ann) {
                                cb(null, null);
                            })
                    }
                }
            })
        knex('announcement')
            .where({
                ntf_id: ntfid
            })
            .select('hayir')
            .options({ rowMode: 'array' })
            .then(function(nayir) {
                console.log(nayir[0][0])
                if (nayir[0][0] == null) {
                    if (hayir == 'hayir') {
                        knex('announcement')
                            .where({
                                ntf_id: ntfid
                            })
                            .update({
                                hayir: knex.raw('array_append(hayir,?)', [id])
                            }).then(function(ann) {
                                cb(null, null);
                            })

                    }
                } else if (nayir[0][0].indexOf(id) == -1) {
                    if (hayir == 'hayir') {
                        knex('announcement')
                            .where({
                                ntf_id: ntfid
                            })
                            .update({
                                hayir: knex.raw('array_append(hayir,?)', [id])
                            }).then(function(ann) {
                                cb(null, null);
                            })
                    }
                }
            })
    })
}

exports.postComment = function(user_id, name, text, anno_id, cb) {
    process.nextTick(function() {
        knex('comment')
            .insert({
                user_id: user_id,
                cmmt_box: text,
                notf_id: anno_id,
                cmmt_creationdate: new Date(),
                writer_name: name
            })
            .then(function(cmt) {
                return cb(null, null)
            })

    })
}

exports.getCommentByID = function(id, cb) {
    process.nextTick(function() {
        knex('comment')
            .where({
                notf_id: id
            })
            .select('*')
            .then(function(commt) {
                cb(null, commt);
            })
    })
}

exports.addToAnnList = function(id, user_id, cb) {
    process.nextTick(function() {
        knex('announcement').where({
                ntf_id: id
            }).select('list')
            .options({ rowMode: 'array' })
            .then(function(list) {
                    if (list[0][0] == null) {
                        knex('announcement')
                            .where({
                                ntf_id: id
                            })
                            .update({
                                list: knex.raw('array_append(list,?)', [user_id])
                            }).then(function(ann) {
                                cb(null, null);
                            })
                    } else if (list[0][0].indexOf(user_id) == -1) {
                        knex('announcement')
                            .where({
                                ntf_id: id
                            })
                            .update({
                                list: knex.raw('array_append(list,?)', [user_id])
                            }).then(function(ann) {
                                cb(null, null);
                            })
                    } else {
                        cb(null, null);
                    }
                }

            )
    })
}

exports.isTeacher = function(user_id, cb) {
    process.nextTick(function() {
        knex('teacher').where({
                t_id: user_id
            }).select('t_id')
            .then(function(id) {
                if (id.length == 0) {
                    return cb(null, false);
                }
                return cb(null, true);

            })
    })
}

exports.getList = function(id, cb) {
    process.nextTick(function() {
        knex('announcement').where({
                ntf_id: id
            }).select('list')
            .then(function(list) {
                return cb(null, list[0].list);
            })
    })
}

exports.getEvetList = function(id, cb) {
    process.nextTick(function() {
        knex('announcement').where({
                ntf_id: id
            }).select('evet')
            .then(function(list) {
                console.log(list[0].evet)
                return cb(null, list[0].evet);
            })
    })
}

exports.getHayirList = function(id, cb) {
    process.nextTick(function() {
        knex('announcement').where({
                ntf_id: id
            }).select('hayir')
            .then(function(list) {
                console.log(list[0].hayir)
                return cb(null, list[0].hayir);
            })
    })
}

exports.findList = function(list, id, cb) {
    process.nextTick(function() {
        var array = [];
        var i = 0;
        if (list == null) {
            cb(null, null)
        } else {
            while (list[i] != undefined) {
                knex('users').where({
                        user_id: list[i]
                    }).select('name', 'surname')
                    .then(function(std) {
                        array.push(std[0])
                    })
                i++;
            }
            setTimeout(function() {
                cb(null, array);
            }, 1000);
        }
    })
}

exports.findOList = function(list, id, cb) {
    process.nextTick(function() {
        var array = [];
        if (list[0] == null) {
            cb(null, null)
        } else {
            list.forEach(function(value) {
                knex('student').where({
                        s_id: value
                    }).select('name', 'surname')
                    .then(function(std) {
                        array.push(std[0].name + " " + std[0].surname);
                        if (list[list.length - 1] == value) {
                            cb(null, array)
                        }
                    })
            })
        }

    })
}


exports.getStudentbyID = function(id, cb) {
    process.nextTick(function() {

        knex('student')
            .where({ s_id: id })
            .select('*')
            .then(function(list) {
                return cb(null, list[0]);
            })

    })
}

exports.getOgrenciDevam = function(sid, cb) {
    process.nextTick(function() {
        knex('bilgilendirme')
            .where({
                oid: sid
            })
            .select('*')
            .then(function(ogrenciDev) {
                return cb(null, ogrenciDev)
            })
    })
}

exports.getStudent = function(id, cb) {
    process.nextTick(function() {

        knex('student')
            .select('*')
            .then(function(list) {
                if (list.length <= id) {
                    return cb(null, null)
                } else {
                    return cb(null, list[id]);
                }

            })

    })
}

exports.getOgrenciOnay = function(cb) {
    process.nextTick(function() {

        knex('student')
            .where({
                onay: '3-A'
            })
            .select('*')
            .then(function(ogrenci) {
                return cb(null, ogrenci)
            })
    })
}

exports.getOgrenci = function(p_id, cb) {
    process.nextTick(function() {

        knex('student')
            .where({
                p_id: p_id
            })
            .select('*')
            .then(function(ogrenci) {
                return cb(ogrenci, null)
            })
    })
}

exports.veliCevapVarMi = function(user_id, not_id, cb) {
    process.nextTick(function() {
        knex('announcement')
            .where({
                ntf_id: not_id
            })
            .select('evet')
            .then(function(evetList) {


                knex('announcement')
                    .where({
                        ntf_id: not_id
                    })
                    .select('hayir')
                    .then(function(hayirList) {
                        console.log(evetList)
                        console.log(hayirList)
                        console.log(evetList[0].evet)
                        if (evetList[0].evet == null && hayirList[0].hayir == null) {
                            console.log("111111")
                            return cb(null, 'false')
                        } else if (evetList[0].evet == null) {
                            console.log("2")
                            if (hayirList[0].hayir.indexOf(user_id) >= 0) {
                                console.log("3")
                                return cb(null, 'true')
                            } else if (hayirList[0].hayir.indexOf(user_id) == -1) {
                                console.log("8")
                                return cb(null, 'false')
                            }
                        } else if (hayirList[0].hayir == null) {
                            console.log("4")
                            if (evetList[0].evet.indexOf(user_id) >= 0) {
                                console.log("5")
                                return cb(null, 'true')
                            } else if (evetList[0].evet.indexOf(user_id) == -1) {
                                console.log("9")
                                return cb(null, 'false')
                            }
                        } else if (evetList[0].evet.indexOf(user_id) >= 0 || hayirList[0].hayir.indexOf(user_id) >= 0) {
                            console.log("6")
                            return cb(null, 'true')
                        } else if (evetList[0].evet.indexOf(user_id) == -1 && hayirList[0].hayir.indexOf(user_id) == -1) {
                            console.log("7")
                            return cb(null, 'false')
                        } else {
                            console.log("assdkjsdkasdjo")
                        }
                    })


            })


    })

}

exports.knex = knex;