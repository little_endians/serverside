var fs = require("fs");
var express = require('express');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var users = require('./user');
var app = express();

var port = process.env.PORT || 5000;

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname));
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    secure: true,
    maxAge: 60000
}));

app.use(passport.initialize());
app.use(passport.session());

app.set('views', './views');
app.set('view engine', 'pug');

passport.use(new Strategy(
    function(username, password, cb) {
        users.findByUsername(username, function(err, user) {
            if (err) {
                return cb(err);
            }
            if (!user) {
                return cb(null, false, { message: 'Incorrect username.' });
            }
            if (user.password != password) {
                return cb(null, false, { message: 'Incorrect password.' });
            }
            return cb(null, user);
        });
    }));

passport.serializeUser(function(user, cb) {
    cb(null, user.user_id);
});

passport.deserializeUser(function(id, cb) {
    users.findById(id, function(err, user) {
        if (err) { return cb(err); }
        cb(null, user);
    });
});

app.get('/duyuru', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {

    res.render('duyuru', {
        user: req.user
    });
});


app.get('/', function(req, res) {
    if (req.user == undefined) {
        res.render('Login');
    } else {
        users.findPuppies(req.user.user_id, 0, function(err, std, clas) {
            if (clas) { //ogretmen
                users.findAnn(clas, function(err, announces) {
                    if (announces) {
                        announces.forEach(function(value) {
                            var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                            var day = value.ntf_creationdate.getUTCDate();
                            var year = value.ntf_creationdate.getUTCFullYear();

                            var saat = value.ntf_creationdate.getHours();
                            var dakika = value.ntf_creationdate.getMinutes();
                            value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                        })
                    }
                    res.render('ogretmen', {
                        user: req.user,
                        announce: announces,
                        classes: clas
                    });
                })
            } else if (std) { //veli
                users.findAnnounces(std.std_id[0], function(err, announces) {
                    users.findNames(std.std_id[0], function(err, names) {
                        if (announces) {
                            announces.forEach(function(value) {
                                var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                                var day = value.ntf_creationdate.getUTCDate();
                                var year = value.ntf_creationdate.getUTCFullYear();

                                var saat = value.ntf_creationdate.getHours();
                                var dakika = value.ntf_creationdate.getMinutes();
                                value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                            })
                        }
                        res.render('duyuru', {
                            user: req.user,
                            announce: announces,
                            student: names
                        });
                    })
                })
            } else { //ogrencisiz veli
                res.render('duyuru', {
                    user: req.user,
                    announce: [],
                    student: []
                });
            }
        })
    }
})


app.post('/Login', passport.authenticate('local', {
        failureRedirect: '/Login'
    }),
    function(req, res) {
        res.redirect('/');
    });

app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

app.get('/yeniDuyuru', function(req, res) {
    res.render('yeniDuyuru');
});

app.get('/ozelDuyuru', function(req, res) {
    res.render('ozelDuyuru');
});

app.post('/yeniDuyuru', function(req, res) {

    users.postAnnounces(req.user.user_id, req.user.name + " " + req.user.surname, req.body.title, req.body.text, new Date(), "tartisma", function(err, announces) {
        res.redirect('/');
    })
})

app.post('/yeniYanitDuyuru', function(req, res) {

    users.postAnnounces(req.user.user_id, req.user.name + " " + req.user.surname, req.body.title, req.body.text, new Date(), "yanit", function(err, announces) {
        res.redirect('/');
    })
})

app.get('/sinifSec', function(req, res) {

    res.render('sinifSec', {
        user: req.user
    });

})

var dersSaati = '';

app.post('/sinifSec', function(req, res) {
    dersSaati = req.body.saat;

    users.getStudent(0, function(err, ogrenci) {
        bionceki = ogrenci.s_id;
        yoklamaListesi = [];
        res.render('yoklama', {
            ogrenci: ogrenci,
            no: 0,
            user: req.user
        });
    })

})

var yoklama = [];
var yoklamaListesi = [];
var bionceki;
app.get('/yoklama/:id/:vy', function(req, res) {

    if (req.params.vy == 0) {
        yoklama.push(bionceki);
        users.getStudentbyID(bionceki, function(err, ogrenci) {
            yoklamaListesi.push(ogrenci);
        })
    }

    users.getStudent(req.params.id, function(err, ogrenci) {
        if (ogrenci) {
            bionceki = ogrenci.s_id;
            res.render('yoklama', { ogrenci: ogrenci, no: req.params.id });
        } else {
            yoklama = [];
            bionceki = -1;
            setTimeout(function() {
                res.render('yListesi', {
                    yoklama: yoklamaListesi,
                    user: req.user
                });
            }, 500);

        }

    })

})

app.get('/yoklamaOnay', function(req, res) {

    var yoklar = [];

    yoklamaListesi.forEach(function(value) {
        users.devamsizlik(value.s_id, new Date(), dersSaati, function(err, devam) {})
        yoklar.push(value.s_id)
    })

    users.devamsizlikList(yoklar, new Date(), dersSaati, function(err, devam) {})
    res.redirect('/');


})

app.post('/ozelDuyuru', function(req, res) {

    ///OZELLESECEK

    var text = "";
    if (req.body.tur == 'Gezi') {
        text = "Okulunuz anasınıfı öğretmeniyim. " + req.body.tarih + " tarihinde, " + req.body.baslangic + " - " + req.body.bitis + " saatleri arasında " + req.body.konum + "’ne gezi düzenlemek istiyorum. " + "\n" + "Çocuğunuzun katılım durumunuzu lütfen en kısa süre içinde bildiriniz." + "\n" + "Gereğini saygılarımla arz ederim."
    } else if (req.body.tur == 'Veli Toplantısı') {
        text = "Okulunuz anasınıfı öğretmeniyim. " + req.body.tarih + " tarihinde, " + req.body.baslangic + " - " + req.body.bitis + " saatleri arasında gündem maddeleri doğrultusunda veli toplantısı yapmak istiyorum. " + "\n" + "Katılım durumunuzu lütfen en kısa süre içinde bildiriniz." + "\n" + "Gereğini saygılarımla arz ederim."
    }

    users.postAnnounces(req.user.user_id, req.user.name + " " + req.user.surname, req.body.tur, text, new Date(), "yanit", function(err, announces) {
        res.redirect('/');
    })


})

app.post('/yeniComment/:id', function(req, res) {

    users.postComment(req.user.user_id, req.user.name + " " + req.user.surname, req.body.text, req.params.id, function(err, comment) {
        users.getCommentType(req.params.id, function(err, type) {
            if (type == 'soru') {
                res.redirect('/sorular/' + req.params.id);
            } else {
                res.redirect('/duyurular/' + req.params.id);
            }
        })
    })
})

app.get('/duyurular/:id', function(req, res) {
    users.getAnnByID(req.params.id, function(err, announces) {
        users.getCommentByID(req.params.id, function(err, comments) {
            users.addToAnnList(req.params.id, req.user.user_id, function(err, ann) {
                users.isTeacher(req.user.user_id, function(err, bool) {
                    users.veliCevapVarMi(req.user.user_id, req.params.id, function(err, cevapVarMi) {
                        console.log(cevapVarMi)
                        if (announces) {
                            announces.forEach(function(value) {
                                var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                                var day = value.ntf_creationdate.getUTCDate();
                                var year = value.ntf_creationdate.getUTCFullYear();

                                var saat = value.ntf_creationdate.getHours();
                                var dakika = value.ntf_creationdate.getMinutes();
                                value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                            })
                        }
                        if (comments) {
                            comments.forEach(function(value) {
                                var month = value.cmmt_creationdate.getUTCMonth() + 1; //months from 1-12
                                var day = value.cmmt_creationdate.getUTCDate();
                                var year = value.cmmt_creationdate.getUTCFullYear();

                                var saat = value.cmmt_creationdate.getUTCHours();
                                var dakika = value.cmmt_creationdate.getUTCMinutes();
                                value.cmmt_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                            })
                        }
                        res.render('goruntule', {
                            user: req.user,
                            comment: comments,
                            announce: announces,
                            list: bool,
                            cevapVarMi: cevapVarMi
                        })
                    })
                })
            })
        })
    })
})

app.get('/sorular/:id', function(req, res) {
    users.getAnnByID(req.params.id, function(err, announces) {
        users.getCommentByID(req.params.id, function(err, comments) {
            if (announces) {
                announces.forEach(function(value) {
                    var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                    var day = value.ntf_creationdate.getUTCDate();
                    var year = value.ntf_creationdate.getUTCFullYear();

                    var saat = value.ntf_creationdate.getHours();
                    var dakika = value.ntf_creationdate.getMinutes();
                    value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                })
            }
            if (comments) {
                comments.forEach(function(value) {
                    var month = value.cmmt_creationdate.getUTCMonth() + 1; //months from 1-12
                    var day = value.cmmt_creationdate.getUTCDate();
                    var year = value.cmmt_creationdate.getUTCFullYear();

                    var saat = value.cmmt_creationdate.getUTCHours();
                    var dakika = value.cmmt_creationdate.getUTCMinutes();
                    value.cmmt_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                })
            }

            res.render('soruGoruntule', {
                user: req.user,
                comment: comments,
                announce: announces,
            })
        })
    })
})

app.get('/duyuruSil/:id', function(req, res) {

    users.deleteAnnByID(req.params.id, function(err, announces) {
        res.redirect('/');
    })

})

app.get('/duyuruDuzenle/:id', function(req, res) {

    users.getAnnByID(req.params.id, function(err, announces) {

        if (announces) {
            announces.forEach(function(value) {
                var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                var day = value.ntf_creationdate.getUTCDate();
                var year = value.ntf_creationdate.getUTCFullYear();

                var saat = value.ntf_creationdate.getHours();
                var dakika = value.ntf_creationdate.getMinutes();
                value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
            })
        }

        res.render('edit', {
            user: req.user,
            announce: announces
        });
    })
})

app.post('/duyuruDuzenle/:id', function(req, res) {

    users.editAnnByID(req.params.id, req.body.title, req.body.text, function(err, announces) {
        res.redirect('/');
    })

})

app.get('/kimGordu/:id', function(req, res) {

    users.getList(req.params.id, function(err, list) {
        users.findList(list, req.params.id, function(err, arr) {
            res.render('kimGordu', {
                user: req.user,
                list: arr
            });
        })
    })
})

app.get('/cevaplar/:id', function(req, res) {

    users.getEvetList(req.params.id, function(err, evet) {
        users.getHayirList(req.params.id, function(err, hayir) {
            users.findList(evet, req.params.id, function(err, cevapEvet) {
                users.findList(hayir, req.params.id, function(err, cevapHayir) {
                    res.render('cevaplar', {
                        user: req.user,
                        evet: cevapEvet,
                        hayir: cevapHayir
                    });
                })
            })
        })
    })
})

app.get('/soruSor', function(req, res) {

    //sinifa giren ogretmenler
    //sinif 3/A
    res.render('soru', {
        user: req.user
    });

})

app.post('/soruSor', function(req, res) {

    users.postQuestion(req.user.user_id, req.user.name + " " + req.user.surname, req.body.teacher, req.body.title, req.body.text, new Date(), "soru", function(err, announces) {
        res.redirect('/sorulariGor');
    })

})

app.get('/sorulariGor', function(req, res) {

    //sinifa giren ogretmenler
    //sinif 3/A
    users.isTeacher(req.user.user_id, function(err, bool) {
        if (bool) {
            users.findQuestionT(req.user.user_id, function(err, announces) {
                if (announces) {
                    announces.forEach(function(value) {
                        var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                        var day = value.ntf_creationdate.getUTCDate();
                        var year = value.ntf_creationdate.getUTCFullYear();

                        var saat = value.ntf_creationdate.getHours();
                        var dakika = value.ntf_creationdate.getMinutes();
                        value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                    })
                }

                res.render('sorulariGor', {
                    user: req.user,
                    announce: announces
                });
            })
        } else {
            users.findQuestion(req.user.user_id, function(err, announces) {
                if (announces) {
                    announces.forEach(function(value) {
                        var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                        var day = value.ntf_creationdate.getUTCDate();
                        var year = value.ntf_creationdate.getUTCFullYear();

                        var saat = value.ntf_creationdate.getHours();
                        var dakika = value.ntf_creationdate.getMinutes();
                        value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                    })
                }

                res.render('sorulariGor', {
                    user: req.user,
                    announce: announces
                });
            })
        }
    })
})

app.get('/bildirileriGor', function(req, res) {

    users.findBildirim(req.user.user_id, function(err, announces) {
        res.render('bildirileriGor', {
            user: req.user,
            announce: announces
        });
    })
})


// Parent SignUp db bağlantısı begin
app.post('/SignUpParent', function(req, res) {

    var email = req.body.email;
    var pass = req.body.pass;
    var name = req.body.name;
    var surname = req.body.surname;
    var phonenumber = req.body.phonenumber;
    var bool = false;

    users.knex('users')
        .insert({ name: name, surname: surname, password: pass, phonenumber: phonenumber, email: email })
        .returning('user_id')
        .then(function(user_id) {

            users.knex('parent')
                .insert({
                    p_id: user_id[0],
                    std_id: []
                })
                .catch(function(error) {
                    bool = true;
                });
        })
        .catch(function(error) {
            bool = true;
        });

    setTimeout(function() {
        if (bool) {
            res.render('Login', { error: true })
        } else {
            res.render('Login', { error: false })
        }
    }, 500);

});
// Parent SignUp db bağlantısı end


// TeacherSignUp verileri db bağlantısı begin

app.post('/SignUpTeacher', function(req, res) {

    var email = req.body.email;
    var pass = req.body.pass;
    var name = req.body.name;
    var surname = req.body.surname;
    var phonenumber = req.body.phonenumber;
    var bool = false;

    user_id = users.knex('users')
        .insert({ name: name, surname: surname, password: pass, phonenumber: phonenumber, email: email })
        .returning('user_id')
        .then(function(user_id) {
            users.knex('teacher')
                .insert({ t_id: user_id[0] })
                .catch(function(error) {
                    bool = true;
                });
        })
        .catch(function(error) {
            bool = true;
        });

    setTimeout(function() {
        if (bool) {
            res.render('Login', { error: true })
        } else {
            res.render('Login', { error: false })
        }
    }, 500);

});
// TeacherSignUp verileri db bağlantısı end

//OgrenciEkle verileri db bağlantısı begin

app.post('/OgrenciEkle', function(req, res) {

    var studentname = req.body.studentname;
    var studentsurname = req.body.studentsurname;
    var classname = req.body.classname;
    var id = req.session.passport.user;

    users.knex('student')
        .insert({
            name: studentname,
            surname: studentsurname,
            onay: classname,
            p_id: id
        })
        .returning('s_id')
        .then(function(s_id) {

            users.knex('parent')
                .where({ p_id: id })
                .select('*')
                .then(function(sut) {
                    console.log(!sut[0].std_id)
                    if (sut[0].std_id) {
                        users.knex('parent')
                            .where({ p_id: id })
                            .update({
                                std_id: users.knex.raw('array_append(std_id,?)', [s_id[0]])
                            })
                            .then(function(e) {
                                console.log(s_id)
                                res.redirect('/');
                            })

                    } else {
                        users.knex('parent')
                            .where({ p_id: id })
                            .update({
                                std_id: s_id[0]
                            })
                            .then(function(e) {
                                console.log(s_id)
                                res.redirect('/');
                            })
                    }
                })
        })
});

//OgrenciEkle verileri db bağlantısı end

app.get('/OgrenciOnay', function(req, res) {
    users.getOgrenciOnay(function(err, ogrenci) {
        res.render('OgrenciOnay', {
            ogrenci: ogrenci
        })
    })

});

app.post('/OgrenciOnay/:id', function(req, res) {
    var onay = req.body.Onay;
    var red = req.body.Red;
    var s_idd = req.params.id;
    console.log(onay)
    console.log(red)
    console.log(s_idd)
    if (onay == 'Onayla') {
        console.log(s_idd)
        users.knex('student')
            .where({
                s_id: s_idd
            })
            .update({
                onay: null,
                class_section: '3-A'
            })
            .then(function(osd) {
                res.redirect('/OgrenciOnay');
            })
    } else if (red == 'Reddet') {
        console.log(red)
        users.knex('student')
            .where({
                s_id: s_idd
            })
            .del()
            .then(function(osd) {
                res.redirect('/OgrenciOnay');
            })
    }

});

app.get('/devam', function(req, res) {
    users.getOgrenci(req.user.user_id, function(ogrenci, err) {
        res.render('devam', {
            ogrenci: ogrenci,
            user: req.user
        })
    })

});

app.get('/ogrenciDevam/:id', function(req, res) {
    users.getOgrenciDevam(req.params.id, function(err, ogrenciDev) {
        if (ogrenciDev) {
            ogrenciDev.forEach(function(value) {
                var month = value.tarih.getUTCMonth() + 1; //months from 1-12
                var day = value.tarih.getUTCDate();
                var year = value.tarih.getUTCFullYear();
                value.tarih = day + "/" + month + "/" + year;
            })
        }
        res.render('ogrenciDevam', {
            ogrenciDev: ogrenciDev,
            user: req.user
        })
    })

});

//ŞifreDeğiştir işlemi db bağlantısı begin

app.post('/SifreDegistir', function(req, res) {

    var oldpass = req.body.oldpass;
    var newpass = req.body.newpass;
    var renewpass = req.body.renewpass;
    var bool = false;
    //var parent_id = req.body;
    // console.log(req);
    var id = req.session.passport.user;

    if (newpass != renewpass) {
        res.render('sifreDegistir', { error: 'Yeni şifreler uyuşmuyor. Tekrar giriniz.' })
        return
    }
    users.knex('users')
        .where({ user_id: id })
        .select('password')
        .then(function(pass) {
            console.log(pass);
            if (pass[0].password != oldpass) {
                res.render('SifreDegistir', {
                    error: 'Eski şifre hatalı girilmiştir. Tekrar giriniz.'
                })
            } else {
                users.knex('users')
                    .where({ user_id: id })
                    .update({ password: newpass })
                    .then(console.log("password değiştirildi."))

                res.redirect('/');
            }
        })

});

//ŞifreDeğiştir işlemi db bağlantısı end

//EVET HAYIR

app.post('/yanitDuyuru/:id', function(req, res) {

    var evet;
    var hayir;
    var cevap = req.body.cevap;
    if (cevap == "evet")
        evet = "evet"
    else if (cevap == "hayir")
        hayir = "hayir"



    var id = req.user.user_id;
    var ntfid = req.params.id;
    var password = req.body.pass;
    var pass2;

    users.findById(id, function(err, gelen) {
        pass2 = gelen.password;
        if (pass2 == password) {
            users.oyVer(evet, hayir, id, ntfid, function(err, announces) {
                res.redirect('/duyurular/' + req.params.id);
            })
        } else {
            users.getAnnByID(req.params.id, function(err, announces) {
                users.getCommentByID(req.params.id, function(err, comments) {
                    users.addToAnnList(req.params.id, req.user.user_id, function(err, ann) {
                        users.isTeacher(req.user.user_id, function(err, bool) {
                            if (announces) {
                                announces.forEach(function(value) {
                                    var month = value.ntf_creationdate.getUTCMonth() + 1; //months from 1-12
                                    var day = value.ntf_creationdate.getUTCDate();
                                    var year = value.ntf_creationdate.getUTCFullYear();

                                    var saat = value.ntf_creationdate.getHours();
                                    var dakika = value.ntf_creationdate.getMinutes();
                                    value.ntf_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                                })
                            }
                            if (comments) {
                                comments.forEach(function(value) {
                                    var month = value.cmmt_creationdate.getUTCMonth() + 1; //months from 1-12
                                    var day = value.cmmt_creationdate.getUTCDate();
                                    var year = value.cmmt_creationdate.getUTCFullYear();

                                    var saat = value.cmmt_creationdate.getUTCHours();
                                    var dakika = value.cmmt_creationdate.getUTCMinutes();
                                    value.cmmt_creationdate = day + "/" + month + "/" + year + " " + saat + ":" + dakika;
                                })
                            }
                            res.render('goruntule', {
                                user: req.user,
                                comment: comments,
                                announce: announces,
                                list: bool,
                                error: 'Şifre yanlış cevap onaylanmadı tekrar deneyiniz.'
                            })
                        })
                    })
                })
            })

        }

    })
});

//EVET HAYIR END

app.get('/OgrenciEkle', function(req, res) {
    res.render('OgrenciEkle');
});

app.get('/SifreDegistir', function(req, res) {
    res.render('SifreDegistir');
});

app.get('/duyuruTuruSayfasi', function(req, res) {
    res.render('duyuruTuruSayfasi', {
        user: req.user
    });
});

app.get('/yeniYanitDuyuru', function(req, res) {
    res.render('yeniYanitDuyuru', {
        user: req.user
    });
});

app.get('/login', function(req, res) {
    res.render('Login');
});

app.get('/SignUpParent', function(req, res) {
    res.render('SignUpParent');
});

app.get('/SignUpTeacher', function(req, res) {
    res.render('SignUpTeacher');
});

app.get('/yoklamaGoruntule', function(req, res) {

    users.knex('devamsizliklist')
        .select('*')
        .orderBy('tarih', 'desc')
        .then(function(ders) {
            ders.forEach(function(value) {
                var month = value.tarih.getUTCMonth() + 1; //months from 1-12
                var day = value.tarih.getUTCDate();
                var year = value.tarih.getUTCFullYear();
                value.tarih = day + "/" + month + "/" + year;
            })
            res.render('yoklamaGor', {
                ders: ders
            });
        })

});

app.get('/ders/:id', function(req, res) {
    users.knex('devamsizliklist')
        .where({ did: req.params.id })
        .select('yoklama', 'tarih', 'derssaati')
        .then(function(ders) {
            var month = ders[0].tarih.getUTCMonth() + 1; //months from 1-12
            var day = ders[0].tarih.getUTCDate();
            var year = ders[0].tarih.getUTCFullYear();
            var tarih = day + "/" + month + "/" + year;
            users.findOList(ders[0].yoklama, 0, function(err, seat) {
                res.render('ders', {
                    ders: seat,
                    tarih: tarih + " " + ders[0].derssaati
                });
            })
        })
})

////////////////////////////////////////////////////////////////////////////
///Android icin/////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
app.post('/mlogin', function(req, res) {

    console.log(req.body)

    var email = req.body.email;
    var pass = req.body.password;

    users.knex('users')
        .where({ email: email })
        .then(function(pass_id) {
            console.log(pass_id[0].password)
            if (pass_id[0] == undefined) {
                console.log({ 'response': "User not exist", 'user': null })
                res.json({ 'user': null });
            } else if (pass == pass_id[0].password) {
                users.isTeacher(pass_id[0].user_id, function(err, bool) {
                    console.log({ 'response': "OK", 'user': pass_id[0] })
                    res.json({ 'user': pass_id[0], 'teacher': bool });
                })
            } else {
                console.log({ 'response': "Invalid Password", 'user': null })
                res.json({ 'user': null });
            }
        })

})

app.post('/msignupparent', function(req, res) {

    var email = req.body.email;
    var pass = req.body.pass;
    var name = req.body.name;
    var surname = req.body.surname;
    var phonenumber = req.body.phonenumber;

    console.log(req)

    users.knex('users')
        .insert({ name: name, surname: surname, password: pass, phonenumber: phonenumber, email: email })
        .returning('user_id')
        .then(function(user_id) {
            users.knex('parent')
                .insert({ p_id: user_id[0] })
                .then(
                    res.json({ 'response': "Insertion completed.", 'user_id': user_id[0] })
                )
                .catch(function(error) {
                    res.json({ 'response': "Cannot insert to parent table", 'user_id': 0 });
                });
        })
        .catch(function(error) {
            res.json({ 'response': "Cannot insert to users table", 'user_id': 0 });
        });

})

app.post('/msignupteacher', function(req, res) {

    var email = req.body.email;
    var pass = req.body.pass;
    var name = req.body.name;
    var surname = req.body.surname;
    var phonenumber = req.body.phonenumber;

    console.log(req)

    users.knex('users')
        .insert({ name: name, surname: surname, password: pass, phonenumber: phonenumber, email: email })
        .returning('user_id')
        .then(function(user_id) {
            users.knex('teacher')
                .insert({ t_id: user_id[0] })
                .then(
                    res.json({ 'response': "Insertion completed.", 'user_id': user_id[0] })
                )
                .catch(function(error) {
                    res.json({ 'response': "Cannot insert to teacher table", 'user_id': 0 });
                });
        })
        .catch(function(error) {
            res.json({ 'response': "Cannot insert to users table", 'user_id': 0 });
        });

})

app.get('/mAnnouncements/:id', function(req, res) {

    users.findById(req.params.id, function(err, user) {
        if (user) {
            users.findPuppies(req.params.id, 0, function(err, std, clas) {
                if (clas) {
                    users.findAnn(clas, function(err, announces) {
                        res.json({
                            user: user,
                            announce: announces,
                            classes: clas
                        });
                    })
                } else if (std) {
                    users.findAnnounces(std.std_id[0], function(err, announces) {
                        users.findNames(std.std_id[0], function(err, names) {
                            res.json({
                                user: user,
                                announce: announces,
                                student: names
                            });
                        })
                    })
                } else {
                    res.json({
                        user: user,
                        announce: [],
                        student: []
                    });
                }
            })
        } else {
            res.json({
                user: [],
                announce: [],
                student: []
            });
        }
    })
})

app.post('/newAnn', function(req, res) {

    users.postAnnounces(req.body.user_id, req.body.username, req.body.title, req.body.text, req.body.date, req.body.type, function(err, announces) {
        if (err) {
            res.json({
                error: err
            })
        } else {
            res.json({
                announce: announces
            });
        }
    })
})

app.post('/mozelDuyuru', function(req, res) {

    console.log(req.body)

    var text = "";
    if (req.body.tur == 'Gezi') {
        text = "Okulunuz anasınıfı öğretmeniyim. " + req.body.tarih + " tarihinde, " + req.body.baslangic + " - " + req.body.bitis + " saatleri arasında " + req.body.konum + "’ne gezi düzenlemek istiyorum. " + "\n" + "Çocuğunuzun katılım durumunuzu lütfen en kısa süre içinde bildiriniz." + "\n" + "Gereğini saygılarımla arz ederim."
    } else if (req.body.tur == 'Veli Toplantısı') {
        text = "Okulunuz anasınıfı öğretmeniyim. " + req.body.tarih + " tarihinde, " + req.body.baslangic + " - " + req.body.bitis + " saatleri arasında gündem maddeleri doğrultusunda veli toplantısı yapmak istiyorum. " + "\n" + "Katılım durumunuzu lütfen en kısa süre içinde bildiriniz." + "\n" + "Gereğini saygılarımla arz ederim."
    }

    console.log(text)

    users.postAnnounces(req.body.user_id, req.body.username, req.body.tur, text, req.body.date, "yanit", function(err, announces) {
        res.json({
            announce: announces
        });
    })


})

app.get('/mduyurular/:id', function(req, res) {
    users.getAnnByID(req.params.id, function(err, announces) {
        users.getCommentByID(req.params.id, function(err, comments) {
            res.json({
                comment: comments
            })
        })
    })
})

app.post('/mduzenle/:id', function(req, res) {

    users.editAnnByID(req.params.id, req.body.title, req.body.text, function(err, announces) {
        res.json({ completed: true });
    })

})

app.post('/mduyuruSil/:id', function(req, res) {

    users.deleteAnnByID(req.params.id, function(err, announces) {
        res.json({ completed: true });
    })

})

app.post('/mduyuruDuzenle/:id', function(req, res) {

    users.editAnnByID(req.params.id, req.body.title, req.body.text, function(err, announces) {
        res.json({ completed: true });
    })

})

app.get('/mkimGordu/:id', function(req, res) {

    users.getList(req.params.id, function(err, list) {
        users.findList(list, req.params.id, function(err, arr) {
            res.json({
                list: arr
            });
        })
    })
})

app.post('/msoruSor', function(req, res) {

    users.postQuestion(req.body.user_id, req.body.username, req.body.teacher, req.body.title, req.body.text, req.body.date, "soru", function(err, announces) {
        res.json({
            announce: announces
        });
    })
})

app.get('/msorulariGor/:user_id', function(req, res) {

    //sinifa giren ogretmenler
    //sinif 3/A
    users.isTeacher(req.params.user_id, function(err, bool) {
        console.log(bool)
        if (bool) {
            users.findQuestionT(req.params.user_id, function(err, announces) {
                res.json({
                    announce: announces
                });
            })
        } else {
            users.findQuestion(req.params.user_id, function(err, announces) {
                res.json({
                    announce: announces
                });
            })
        }
    })
})

app.get('/msorular/:id', function(req, res) {
    users.getCommentByID(req.params.id, function(err, comments) {
        res.json({
            comment: comments[0]
        })
    })
})

app.post('/msoruSil/:id', function(req, res) {

    users.deleteAnnByID(req.params.id, function(err, announces) {
        res.json({ completed: true });
    })

})

app.post('/msoruDuzenle/:id', function(req, res) {

    users.editAnnByID(req.params.id, req.body.title, req.body.text, function(err, announces) {
        res.json({ completed: true });
    })

})

app.post('/myanitDuyuru/:id', function(req, res) {

    var cevap = req.body.answer;
    var id = req.body.user_id;
    var ntfid = req.params.id;
    var password = req.body.pass;
    var pass2;

    console.log(req.body.pass)

    users.findById(id, function(err, gelen) {
        pass2 = gelen.password;
        console.log(gelen)
        if (pass2 == password) {
            users.oyVer(cevap, cevap, id, ntfid, function(err, announces) {
                res.json({
                    response: 'OK'
                });
            })
        } else {
            res.json({
                response: 'wrong password'
            })
        }
    })
});

app.get('/mcevaplar/:id', function(req, res) {

    users.getEvetList(req.params.id, function(err, evet) {
        users.getHayirList(req.params.id, function(err, hayir) {
            users.findList(evet, req.params.id, function(err, cevapEvet) {
                users.findList(hayir, req.params.id, function(err, cevapHayir) {
                    res.json({
                        evet: cevapEvet,
                        hayir: cevapHayir
                    });
                })
            })
        })
    })
})

app.get('/msinifSec/:sinif', function(req, res) {

    users.knex('student')
        .where({ class_section: req.params.sinif })
        .select('*')
        .then(function(liste) {
            yoklamaListesi = [];
            yoklama = [];
            res.json({
                ogrenci: liste
            });
        })

})

app.post('/myoklama/:id', function(req, res) {

    console.log(req.body)

    dersSaati = req.params.id + ". Saat"

    req.body.forEach(function(value) {
        yoklamaListesi.push(value);
    })

    console.log(yoklamaListesi)

    res.json({
        completed: true
    });

})

app.get('/myoklamalistesi', function(req, res) {

    res.json({
        yoklama: yoklamaListesi
    });

})

app.get('/myoklamaOnay', function(req, res) {

    var yoklar = [];

    yoklamaListesi.forEach(function(value) {
        users.devamsizlik(value.s_id, new Date(), dersSaati, function(err, devam) {})
        yoklar.push(value.s_id)
    })

    console.log(yoklar)

    users.devamsizlikList(yoklar, new Date(), dersSaati, function(err, devam) {})
    res.json({
        completed: true
    })

})

app.post('/myorum/:id', function(req, res) {

    users.postComment(req.body.user_id, req.body.name, req.body.text, req.params.id, function(err, comment) {
        res.json({ completed: true });
    })
})

app.post('/mogrenciEkle', function(req, res) {

    var studentname = req.body.name;
    var studentsurname = req.body.surname;
    var classname = req.body.classname;
    var id = req.body.user_id;

    users.knex('student')
        .insert({
            name: studentname,
            surname: studentsurname,
            onay: classname,
            p_id: id
        })
        .returning('s_id')
        .then(function(s_id) {

            users.knex('parent')
                .where({ p_id: id })
                .select('*')
                .then(function(sut) {
                    console.log(!sut[0].std_id)
                    if (sut[0].std_id) {
                        users.knex('parent')
                            .where({ p_id: id })
                            .update({
                                std_id: users.knex.raw('array_append(std_id,?)', [s_id[0]])
                            })
                            .then(function(e) {
                                res.json({ completed: true });
                            })
                    } else {
                        users.knex('parent')
                            .where({ p_id: id })
                            .update({
                                std_id: s_id[0]
                            })
                            .then(function(e) {
                                res.json({ completed: true });
                            })
                    }
                })
        })
});

app.get('/mogrenciListesi', function(req, res) {
    users.getOgrenciOnay(function(err, ogrenci) {
        res.json({
            ogrenci: ogrenci
        })
    })

});

app.post('/mogrenciOnay/:id', function(req, res) {
    var onay = req.body.Onay;
    var red = req.body.Red;
    var s_idd = req.params.id;
    console.log(onay)
    console.log(red)
    console.log(s_idd)
    if (onay == 'Onayla') {
        console.log(s_idd)
        users.knex('student')
            .where({
                s_id: s_idd
            })
            .update({
                onay: null,
                class_section: '3-A'
            })
            .then(function(osd) {
                res.json({ completed: true });
            })
    } else if (red == 'Reddet') {
        console.log(red)
        users.knex('student')
            .where({
                s_id: s_idd
            })
            .del()
            .then(function(osd) {
                res.json({ completed: true });
            })
    }
});

app.get('/mogrenciler/:id', function(req, res) {
    var ogrenci = [];

    users.getOgrenci(req.params.id, function(std, err) {
        console.log(std)
        res.json({ ogrenci: std })
    })

})

app.get('/mogrenciDevam/:id', function(req, res) {
    users.getOgrenciDevam(req.params.id, function(err, ogrenciDev) {
        res.json({
            ogrenciDev: ogrenciDev
        })
    })

});

app.listen(port);
console.log('Server started! At http://localhost:' + port);